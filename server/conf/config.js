var config = {
	development : {
		url : 'http://ds037195.mlab.com:37195',
        MONGODB_URI:'mongodb://ak:akhilesh@ds113915.mlab.com:13915/akhilesh',
        Local_MONGODB_URI:'mongodb://localhost:27017/akhilesh',
		akDatabase : {
			host : 'localhost',
			user : 'ak',
			password : 'akhilesh',
			database : 'akhilesh',
			connectionLimit : 100, //important
			debug    :  false
		},
		//server details
		server : {
			host : '127.0.0.1',
			port : '8899'
		},
        secret:'myNameIsAkhilesh'
	},
	qa : {
		//url to be used in link generation
		url : 'http://ds037195.mlab.com:37195',
        MONGODB_URI:'mongodb://ak:akhilesh@ds113915.mlab.com:13915/akhilesh',
        Local_MONGODB_URI:'mongodb://localhost:27017/akhilesh',
		akDatabase : {
			host : 'localhost',
			user : 'ak',
			password : 'akhilesh',
			database : 'akhilesh',
			connectionLimit : 100, //important
			debug    :  false
		},
		//server details
		server : {
			host : '127.0.0.1',
			port : '8899'
		},
        secret:'myNameIsAkhilesh'
	},
	production : {
		url : 'http://ds037195.mlab.com:37195',
        MONGODB_URI:'mongodb://ak:akhilesh@ds113915.mlab.com:13915/akhilesh',
        Local_MONGODB_URI:'mongodb://localhost:27017/akhilesh',
		akDatabase : {
			host : 'localhost',
			user : 'ak',
			password : 'akhilesh',
			database : 'akhilesh',
			connectionLimit : 100, //important
			debug    :  false
		},
		//server details
		server : {
			host : '127.0.0.1',
			port : '8899'
		},
        secret:'myNameIsAkhilesh'
	}
};

module.exports = config;
